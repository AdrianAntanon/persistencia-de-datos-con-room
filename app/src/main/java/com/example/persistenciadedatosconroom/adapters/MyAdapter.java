package com.example.persistenciadedatosconroom.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.persistenciadedatosconroom.R;
import com.example.persistenciadedatosconroom.database.Stats;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private final List<Stats> stats;
    private final int layout;


    public MyAdapter(List<Stats> stats, int layout) {
        this.stats = stats;
        this.layout = layout;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(this.layout,parent,false);
        ViewHolder vh= new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.bind(this.stats.get(position));
    }

    @Override
    public int getItemCount() {
        return this.stats.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        public TextView itemViewName;
        public TextView itemViewStats;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            this.itemViewName= itemView.findViewById(R.id.itemViewName);
            this.itemViewStats= itemView.findViewById(R.id.itemViewStats);
        }

        public void bind(final Stats stats){

            this.itemViewName.setText(stats.getPlayer_name());
            String playerStats = "";
            if (stats.getStats() == 1){
                playerStats = stats.getStats()+" punto ";
            }else {
                playerStats = stats.getStats()+" puntos";
            }

            this.itemViewStats.setText(playerStats);
        }
    }
}