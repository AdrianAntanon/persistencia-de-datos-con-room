package com.example.persistenciadedatosconroom.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Stats {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_stats")
    private int id_stats;
    private final int stats;
    private final String player_name;

    public Stats(int stats, String player_name) {
        this.stats = stats;
        this.player_name = player_name;
    }

    public int getId_stats() {
        return id_stats;
    }

    public void setId_stats(int id_stats) {
        this.id_stats = id_stats;
    }

    public int getStats() {
        return stats;
    }

    public String getPlayer_name() {
        return player_name;
    }

}
