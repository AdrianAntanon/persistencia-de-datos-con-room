package com.example.persistenciadedatosconroom.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface DAOQuestion {

    @Query("SELECT * FROM Question")
    List<Question> getAll();

    @Query("SELECT * FROM Question WHERE id_question = :id")
    Question findById(int id);

    @Insert
    void insert(Question question);
}
