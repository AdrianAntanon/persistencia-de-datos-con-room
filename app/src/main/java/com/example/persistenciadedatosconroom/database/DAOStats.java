package com.example.persistenciadedatosconroom.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface DAOStats {
    @Query("SELECT * FROM Stats")
    List<Stats> getAll();

    @Query("SELECT * fROM Stats WHERE id_stats = :id")
    Stats findById(int id);

    @Query("DELETE fROM Stats")
    void deleteAll();

    @Insert
    void insert(Stats stats);

    @Update
    void update(Stats stats);
}
