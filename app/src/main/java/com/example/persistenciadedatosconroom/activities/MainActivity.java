package com.example.persistenciadedatosconroom.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.persistenciadedatosconroom.R;
import com.example.persistenciadedatosconroom.database.AppDataBase;
import com.example.persistenciadedatosconroom.database.DAOQuestion;
import com.example.persistenciadedatosconroom.database.DAOStats;
import com.example.persistenciadedatosconroom.database.Question;
import com.example.persistenciadedatosconroom.database.Stats;
import com.example.persistenciadedatosconroom.repositories.QuestionRepository;
import com.example.persistenciadedatosconroom.repositories.StatsRepository;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    static List<Question> questions;
    static List<Stats> stats;
    static AppDataBase dataBase;
    static DAOQuestion daoQuestion;
    static DAOStats daoStats;
    static QuestionRepository questionRepository;
    static StatsRepository statsRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonStartGame, buttonGameStats;

        buttonStartGame = findViewById(R.id.startGame);
        buttonGameStats = findViewById(R.id.gameStats);

        dataBase = AppDataBase.getInstance(this);

        daoStats = dataBase.daoStats();
        statsRepository = new StatsRepository(daoStats);
        stats = statsRepository.getAll();

        daoQuestion = dataBase.daoQuestion();
        questionRepository = new QuestionRepository(daoQuestion);
        loadQuestions();
        questions = questionRepository.getAll();

        buttonGameStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (stats.size()==0){
                    Toast.makeText(MainActivity.this,"Todavía nadie se ha atrevido con el quiz del metal",Toast.LENGTH_SHORT).show();
                }else{
                    Intent intent= new Intent(MainActivity.this,StatsActivity.class);
                    startActivity(intent);
                }
            }
        });

        buttonStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(MainActivity.this,GameActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        stats =statsRepository.getAll();
    }

    public void loadQuestions(){
        questionRepository.insert(new Question("¿Grupo de metal industrial más conocido del mundo?","Rammstein"));
        questionRepository.insert(new Question("¿Cómo se llama el cantante de Iron Maiden?","Bruce Dickinson"));
        questionRepository.insert(new Question("¿Qué grupo es considerado el creador del género metal?","Black Sabbath"));
        questionRepository.insert(new Question("¿Grupo más famoso del Big Four?","Metallica"));
        questionRepository.insert(new Question("¿De qué país son originarios los hermanos 'Young' de AC/DC?","Australia"));
        questionRepository.insert(new Question("En la película ‘Ace Ventura: Detective de Animales’, ¿qué grupo hizo una pequeña aparición?","Cannibal Corpse"));
        questionRepository.insert(new Question("Grupo de black metal responsable de la quema de iglesias en Noruega","Mayhem"));
        questionRepository.insert(new Question("La mascota del grupo es conocida como Vic Rattlehead, ¿de qué grupo hablamos? ","Megadeth"));
        questionRepository.insert(new Question("Nombre del cantante de Mötorhead","Lemmy Kilmister"));
        questionRepository.insert(new Question("Mötorhead, Megadeth, Metallica, Slayer, uno de ellos no es del big four, ¿qué grupo?","Mötorhead"));
    }
}