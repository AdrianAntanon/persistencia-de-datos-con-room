package com.example.persistenciadedatosconroom.activities;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.persistenciadedatosconroom.R;
import com.example.persistenciadedatosconroom.adapters.MyAdapter;


public class StatsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);

        RecyclerView recyclerView= findViewById(R.id.recyclerView);

        MyAdapter myAdapter = new MyAdapter(MainActivity.stats, R.layout.item_view);

        recyclerView.setAdapter(myAdapter);

        RecyclerView.LayoutManager layout = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layout);


    }
}
