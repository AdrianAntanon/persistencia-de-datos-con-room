package com.example.persistenciadedatosconroom.activities;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.persistenciadedatosconroom.R;
import com.example.persistenciadedatosconroom.database.Question;
import com.example.persistenciadedatosconroom.database.Stats;

import java.util.ArrayList;
import java.util.List;


public class GameActivity extends AppCompatActivity {
    private TextView questionTV, timerView;
    private CountDownTimer countDownTimer;
    private EditText playerNameET;
    private Button startButton;
    private Question question;


    private int counter = 0, score = 0;
    private final List<Integer> amountOfRandoms = new ArrayList<Integer>();
    private String playerName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        questionTV = findViewById(R.id.questionTV);
        playerNameET = findViewById(R.id.player_nameET);
        startButton = findViewById(R.id.start_button);


        timerView = findViewById(R.id.timerView);

        playerNameET.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66 && counter > 0 && event.getAction() == 0) {

                    countDownTimer.cancel();

                    if (counter == 5) {
                        checkQuestion();

                        String turnBack = "volver";
                        startButton.setText(turnBack);

                        String playerScore = "Felicidades, has conseguido ";

                        if (score == 1){
                            playerScore += score + " punto!";
                        }else if (score == 0){
                            playerScore = "Más suerte la próxima vez, no has acertado ninguna :-(";
                        }else {
                            playerScore += score + " puntos!";

                        }
                        questionTV.setText(playerScore);
                        timerView.setVisibility(View.INVISIBLE);
                        playerNameET.setVisibility(View.INVISIBLE);
                        startButton.setVisibility(View.VISIBLE);

                    } else {
                        checkQuestion();
                        nextQuestion();
                    }

                    return true;
                }

                return false;
            }
        });

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (playerNameET.getText().length() > 0) {

                    switch (counter) {
                        case 0:
                            playerName = playerNameET.getText() + "";
                            playerNameET.setHint("Respuesta");
                            nextQuestion();
                            questionTV.setVisibility(View.VISIBLE);
                            startButton.setVisibility(View.INVISIBLE);
                            break;
                        case 5:
                            MainActivity.statsRepository.insert(new Stats(score, playerName));
                            counter = 0;
                            score = 0;
                            finish();
                            break;
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Introduce nombre de jugador, por favor.", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    public void nextQuestion() {
        startTimer();

        int randomNumber = (int) Math.floor(Math.random() * MainActivity.questions.size() + 1);

        if (amountOfRandoms.contains(randomNumber)) {
            while (amountOfRandoms.contains(randomNumber)) {
                randomNumber = (int) Math.floor(Math.random() * MainActivity.questions.size() + 1);
            }
        }

        question = MainActivity.questionRepository.findById(randomNumber);
        questionTV.setText(question.getQuestion());
        playerNameET.setText("");

        amountOfRandoms.add(randomNumber);

        counter += 1;
    }

    public void startTimer() {

        countDownTimer = new CountDownTimer(15000, 1000) {

            public void onTick(long millisUntilFinished) {
                String countTimer = "Cuenta atrás: " + (millisUntilFinished / 1000);

                timerView.setText(countTimer);
            }

            public void onFinish() {
                checkQuestion();
                nextQuestion();
            }
        }.start();

    }

    public void checkQuestion() {
        if (question.getAnswer().trim().equalsIgnoreCase(playerNameET.getText() + "".trim())) {
            score += 1;
        }

    }
}
