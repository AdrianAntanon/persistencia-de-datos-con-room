package com.example.persistenciadedatosconroom.repositories;

import com.example.persistenciadedatosconroom.database.DAOQuestion;
import com.example.persistenciadedatosconroom.database.Question;

import java.util.List;

public class QuestionRepository {

    DAOQuestion daoQuestion;

    public QuestionRepository(DAOQuestion daoQuestion) {
        this.daoQuestion = daoQuestion;
    }

    public Question findById(int id) {
        return daoQuestion.findById(id);
    }

    ;

    public void insert(Question p) {
        daoQuestion.insert(p);
    }

    public List<Question> getAll() {
        return daoQuestion.getAll();
    }

    ;

}
