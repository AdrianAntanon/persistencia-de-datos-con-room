package com.example.persistenciadedatosconroom.repositories;

import com.example.persistenciadedatosconroom.database.DAOStats;
import com.example.persistenciadedatosconroom.database.Stats;

import java.util.List;

public class StatsRepository {

    DAOStats daoStats;

    public StatsRepository(DAOStats daoStats) {
        this.daoStats = daoStats;
    }

    public void insert(Stats p) {
        daoStats.insert(p);
    }

    public List<Stats> getAll() {
        return daoStats.getAll();
    }

}
